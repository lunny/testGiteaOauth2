# Example for Gitea login

## steps

* run your gitea on http://localhost:3000
* go to your settings and add an oauth2 application
* change `ClientID` and `ClientSecret` on the `main.go`
* `go build` and run the program
* visit http://localhost:8000 on your webbrowser, first time you should be prompt to confirm the authorize
* and then you can redirect to your http://localhost:8000 and you can see your user name and email on your gitea instance.